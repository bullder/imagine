<?php

return [
    'Symfony\Bundle\FrameworkBundle\FrameworkBundle' => ['all' => true],
    'Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle' => ['all' => true],
    'Doctrine\Bundle\DoctrineBundle\DoctrineBundle' => ['all' => true],
    'Symfony\Bundle\TwigBundle\TwigBundle' => ['all' => true],
    'Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle' => ['all' => true],
];
