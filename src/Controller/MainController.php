<?php

namespace App\Controller;

use App\Entity\Log;
use App\Model\LogCriteria;
use App\Services\LogService;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function default(): Response
    {
        return $this->render('list.html.twig', ['logs' => $this->getDoctrine()->getRepository(Log::class)->findAll()]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(): Response
    {
        return $this->render('image.html.twig');
    }

    /**
     * @param Request $request
     * @param LogService $logService
     * @throws \LogicException
     * @return BinaryFileResponse
     */
    public function banner(Request $request, LogService $logService): BinaryFileResponse
    {
        $logService->count($request);

        return (new BinaryFileResponse('banner.jpg'))->setExpires((new \DateTime())->modify('-1 month'));
    }

}