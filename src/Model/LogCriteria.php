<?php
namespace App\Model;

use Symfony\Component\HttpFoundation\Request;

class LogCriteria
{
    /**
     * @ORM\Column(type="string")
     */
    public $ip;

    /**
     * @ORM\Column(type="string")
     */
    public $userAgent;

    /**
     * @ORM\Column(type="string")
     */
    public $url;

    /**
     * LogCriteria constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->ip = $request->getClientIp();
        $this->url = $request->headers->get('Referer');
        $this->userAgent = $request->headers->get('User-Agent');
    }

    public function toArray()
    {
        return [
            'ip' => $this->ip,
            'userAgent' => $this->userAgent,
            'url' => $this->url
        ];
    }
}