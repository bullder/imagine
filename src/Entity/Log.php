<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Request;

/**
 * @ORM\Entity
 * @ORM\Table(name="log")
 */
class Log
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="string")
     */
    public $ip;

    /**
     * @ORM\Column(type="string")
     */
    public $userAgent;

    /**
     * @ORM\Column(type="datetime")
     */
    public $datetime;

    /**
     * @ORM\Column(type="string")
     */
    public $url;

    /**
     * @ORM\Column(type="integer")
     */
    public $amount = 1;

    public static function createFromRequest(Request $request): Log
    {
        $log = new static();
        $log->datetime = new \DateTime();
        $log->ip = $request->getClientIp();
        $log->url = $request->headers->get('Referer');
        $log->userAgent = $request->headers->get('User-Agent');

        return $log;
    }

    public function count()
    {
        $this->amount++;
        $this->datetime = new \DateTime();

        return $this;
    }
}