<?php

namespace App\Services;

use App\Entity\Log;
use App\Model\LogCriteria;
use App\Repository\LogRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;

class LogService
{
    /**
     * @var LogRepository
     */
    private $repository;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * LogService constructor.
     * @param Registry $registry
     */
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
        $this->repository = $registry->getRepository(Log::class);
        $this->em = $this->registry->getManagerForClass(Log::class);
    }

    /**
     * @param Request $request
     * @throws \LogicException
     * @return Log
     */
    public function count(Request $request): Log
    {
        $criteria = new LogCriteria($request);
        if (empty($criteria->url) || empty($criteria->ip) || empty($criteria->userAgent)) {
            throw new \LogicException('One field of visitor info is empty: ' . json_encode($criteria));
        }

        $visit = $this->repository->findOneBy($criteria->toArray());

        if ($visit) {
            $visit->count();
        } else {
            $visit = Log::createFromRequest($request);
        }

        $this->em->persist($visit);
        $this->em->flush();

        return $visit;
    }
}