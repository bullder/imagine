<?php
namespace Tests\Services;

use App\Entity\Log;
use App\Repository\LogRepository;
use App\Services\LogService;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class LogServiceTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var LogRepository|\PHPUnit_Framework_MockObject_MockObject
     */
    private $repository;

    /**
     * @var EntityManager|\PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var LogService
     */
    private $service;

    public function setUp()
    {

        $this->em = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $this->repository = $this->getMockBuilder(LogRepository::class)->disableOriginalConstructor()->getMock();

        $registry = $this->getMockBuilder(Registry::class)->disableOriginalConstructor()->getMock();
        $registry->expects($this->once())->method('getRepository')->willReturn($this->repository);
        $registry->expects($this->once())->method('getManagerForClass')->willReturn($this->em);
        $this->service = new LogService($registry);
    }

    public function testCountIncreasing()
    {
        $request = $this->prepareRequest();

        $visit = Log::createFromRequest($request);
        $initialTime = $visit->datetime;
        $this->repository->expects($this->once())->method('findOneBy')->willReturn($visit);
        $this->em->expects($this->once())->method('persist')->with($visit);
        $this->em->expects($this->once())->method('flush');
        $result = $this->service->count($request);
        $this->assertEquals(2, $result->amount);
        $this->assertNotEquals($initialTime, $result->datetime);
    }

    public function testCountCreating()
    {
        $request = $this->prepareRequest();

        $this->repository->expects($this->once())->method('findOneBy')->willReturn(null);
        $this->em->expects($this->once())->method('persist');
        $this->em->expects($this->once())->method('flush');

        $result = $this->service->count($request);
        $this->assertEquals(1, $result->amount);
    }

    public function testCountException()
    {
        $request = $this->prepareRequest();
        $request->server->set('REMOTE_ADDR', null);
        $this->expectException(\LogicException::class);
        $this->service->count($request);
    }

    private function prepareRequest(): Request
    {
        $request = new Request();
        $request->headers->set('Referer', 'some_url');
        $request->headers->set('User-Agent', 'user_agent');
        $request->server->set('REMOTE_ADDR', '127.0.0.1');

        return $request;
    }
}