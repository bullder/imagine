0. Git clone that project
1. Change to the project directory
2. Exec: `composer install`
3. Copy `.env.dist` to `.env`
4. Update db connection `DATABASE_URL` in `.env`
5. Migrate db with `./bin/console doctrine:migrations:migrate`
6. Execute the `make serve` command;
7. Browse to the http://localhost:8000/ URL.
